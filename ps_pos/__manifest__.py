# -*- coding: utf-8 -*-
{
    "name": "PS POS",
    "summary": "PurpleSoft Base",
    "category": "Sales/Point of Sale",
    "author": 'PurpleSoft Solutions',
    "version": "15.0.1.0.0",
    "sequence": 1,
    "license": "Other proprietary",
    "website": "https://purplesoft.odoo.com",
    "description": """
        Point of sale customization.
    """,
    "depends": ['point_of_sale'],
    "data": [],
    "application": True,
    "installable": True,
    "auto_install": False,
    "assets": {
        'point_of_sale.assets': [
            'ps_pos/static/src/css/pos.css',
            'ps_pos/static/src/js/pos.js',
        ],
        'web.assets_qweb': [
            'ps_pos/static/src/xml/**/*',
        ],
    }
}
